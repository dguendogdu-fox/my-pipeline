# Python QA Pipeline

This pipeline guarantees code quality via Pylint and tests via Pytest when a merge request is done.

## Prerequisites
* Docker

## Project structure
To use this pipeline, your project must have following project structure:
* If you do not want to **run** any **tests**, you do not need a test folder.
* If you do not want to **configure** your **pipeline**, you do not need a **.pylintrc** file\
<img src = "images/tree.png">\


## Installation (Follow, if shared runner is not available!)
1. Gitlab-runner installation\
**Debian/Ubuntu**\
`curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
`
`sudo apt-get install gitlab-runner
`
\
<https://docs.gitlab.com/runner/install/linux-repository.html>



2. Gitlab-runner registration\
**Debian/Ubuntu**\
`sudo gitlab-runner register
`
* GitLab **URL** instance: https://gitlab.com/
* Copy your **registration** token (Settings -> CI/CD -> Runners)
* Choose **docker** as executor
* Choose **python:3.8** as default image\
<https://docs.gitlab.com/runner/register/index.html>

## Setup
Copy the  **.gitlab-ci.yml** and **requirement.txt** files from this repository to the **root**
directory of your own project into the master branch.

## Configure Pylint
Too configure pylint copy the **.pylintrc** file to your repository and add your changes.

### For instance
Enable, disable warnings for pylint.
* To enable specific warnings: Add it to **enable**
* To disable specific warnings: Add it to **disable** 
<img src="images/enable_disable_warnings.png" width="500" height="500">


## Usage 
Whenever a **merge request** in your own project is done, the pipeline is executed automatically.

## Notes
* General pipeline architecture choosen for sake of simplicity 
* Create a own gitlab-runner because of missing validation to use shared runners by GitLab
* Pipeline can not be triggered by a external repository because src/ and test/ have to be passed between two unrelated repositories through two independent pipelines

## Issues
* The pipeline only runs on a merge request which is started on GitLab itself
* When merging the feature branch into the master branch, the root files for the pipeline will be merged into the master too
